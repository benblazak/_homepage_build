import{S as t,i as l,s as d,k as o,l as k,m,h as i,n as c,b as f,M as a}from"../chunks/index.edcdfe91.js";const h=`<!-- Created with Inkscape (http://www.inkscape.org/) -->
<svg aria-hidden="true" version="1.1" viewBox="0 0 55.929 50.997" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
 <defs>
  <linearGradient id="linearGradient10744" x1="57.187" x2="80.384" y1="69.294" y2="33.773" gradientTransform="matrix(1.3133 0 0 1.2378 -49.639 -34.774)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient10742"/>
  <linearGradient id="linearGradient10742">
   <stop style="stop-color:#005580" offset="0"/>
   <stop style="stop-color:#001480" offset="1"/>
  </linearGradient>
  <radialGradient id="radialGradient11770" cx="67.961" cy="48.625" r="15.232" gradientTransform="matrix(-6.9009e-7 2.8864 -2 -1.6905e-7 122.71 -189.14)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <linearGradient id="linearGradient12354">
   <stop style="stop-color:#f5fcff" offset="0"/>
   <stop style="stop-color:#fff;stop-opacity:0" offset="1"/>
  </linearGradient>
  <radialGradient id="radialGradient11770-9" cx="56.386" cy="38.008" r="15.232" gradientTransform="matrix(6.501e-8 -2.8864 2 -6.7267e-8 -20.088 213.75)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <linearGradient id="linearGradient10744-9" x1="57.187" x2="80.384" y1="69.294" y2="33.773" gradientTransform="matrix(1.3133 0 0 1.2378 -75.104 -34.774)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient10742"/>
  <radialGradient id="radialGradient11770-0" cx="67.961" cy="48.625" r="15.232" gradientTransform="matrix(-6.9009e-7 2.8864 -2 -1.6905e-7 97.249 -189.14)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <radialGradient id="radialGradient11770-9-8" cx="56.386" cy="38.008" r="15.232" gradientTransform="matrix(6.501e-8 -2.8864 2 -6.7267e-8 -45.552 213.75)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <linearGradient id="linearGradient10744-2" x1="57.187" x2="80.384" y1="69.294" y2="33.773" gradientTransform="matrix(.58983 0 0 .4726 -8.2661 -15.961)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient10742"/>
  <radialGradient id="radialGradient11770-3" cx="67.961" cy="48.625" r="15.232" gradientTransform="matrix(-3.0993e-7 1.1021 -.89824 -6.4545e-8 69.141 -74.897)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <radialGradient id="radialGradient11770-9-0" cx="56.386" cy="38.008" r="15.232" gradientTransform="matrix(2.9197e-8 -1.1021 .89824 -2.5683e-8 5.0061 78.928)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <radialGradient id="radialGradient11770-98" cx="67.961" cy="48.625" r="15.232" gradientTransform="matrix(-3.0993e-7 1.1021 -.89824 -6.4545e-8 60.459 -74.897)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <radialGradient id="radialGradient11770-9-7" cx="56.386" cy="38.008" r="15.232" gradientTransform="matrix(2.9197e-8 -1.1021 .89824 -2.5683e-8 -3.6762 78.928)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient12354"/>
  <linearGradient id="linearGradient1364" x1="57.187" x2="80.384" y1="69.294" y2="33.773" gradientTransform="matrix(.58983 0 0 .4726 -16.948 -15.961)" gradientUnits="userSpaceOnUse" xlink:href="#linearGradient10742"/>
  <mask id="mask1071" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 41.754v-27.467c23.836-16.527 32.288 15.499 6.6877 13.375 34.358-2.6497 15.225 37.455-6.6877 12.77" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1077" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 41.754v-27.467c23.836-16.527 32.288 15.499 6.6877 13.375 34.358-2.6497 15.225 37.455-6.6877 12.77" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1083" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 41.754v-27.467c23.836-16.527 32.288 15.499 6.6877 13.375 34.358-2.6497 15.225 37.455-6.6877 12.77" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1144" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c-23.836-16.527-32.288 15.499-6.6877 13.375-34.358-2.6497-15.225 37.455 6.6877 12.77" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1150" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c-23.836-16.527-32.288 15.499-6.6877 13.375-34.358-2.6497-15.225 37.455 6.6877 12.77" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1156" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c-23.836-16.527-32.288 15.499-6.6877 13.375-34.358-2.6497-15.225 37.455 6.6877 12.77" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1223" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c1.373-3.9983 3.4493-9.4774 8.6824-11.787" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1229" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c1.373-3.9983 3.4493-9.4774 8.6824-11.787" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1235" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c1.373-3.9983 3.4493-9.4774 8.6824-11.787" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1244" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c-1.373-3.9983-3.4493-9.4774-8.6824-11.787" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1250" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c-1.373-3.9983-3.4493-9.4774-8.6824-11.787" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
  <mask id="mask1256" maskUnits="userSpaceOnUse">
   <path class="icon" d="m27.965 14.287c-1.373-3.9983-3.4493-9.4774-8.6824-11.787" style="fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:5;stroke:#fff"/>
  </mask>
 </defs>
 <g>
  <rect x="25.465" y="7.03" width="30.465" height="43.967" mask="url(#mask1083)" style="fill:url(#linearGradient10744);paint-order:fill markers stroke"/>
  <rect x="25.465" y="7.03" width="30.465" height="43.967" mask="url(#mask1077)" style="fill:url(#radialGradient11770);paint-order:fill markers stroke"/>
  <rect x="25.465" y="7.03" width="30.465" height="43.967" mask="url(#mask1071)" style="fill:url(#radialGradient11770-9);paint-order:fill markers stroke"/>
  <rect x="-3.1444e-6" y="7.03" width="30.465" height="43.967" mask="url(#mask1156)" style="fill:url(#linearGradient10744-9);paint-order:fill markers stroke"/>
  <rect x="-3.9564e-6" y="7.03" width="30.465" height="43.967" mask="url(#mask1150)" style="fill:url(#radialGradient11770-0);paint-order:fill markers stroke"/>
  <rect x="-3.1444e-6" y="7.03" width="30.465" height="43.967" mask="url(#mask1144)" style="fill:url(#radialGradient11770-9-8);paint-order:fill markers stroke"/>
  <rect x="25.465" y="-6.1279e-8" width="13.682" height="16.787" mask="url(#mask1235)" style="fill:url(#linearGradient10744-2);paint-order:fill markers stroke"/>
  <rect x="25.465" y="-6.1279e-8" width="13.682" height="16.787" mask="url(#mask1229)" style="fill:url(#radialGradient11770-3);paint-order:fill markers stroke"/>
  <rect x="25.465" y="-6.1279e-8" width="13.682" height="16.787" mask="url(#mask1223)" style="fill:url(#radialGradient11770-9-0);paint-order:fill markers stroke"/>
  <rect x="16.782" y="4.7961e-8" width="13.682" height="16.787" mask="url(#mask1256)" style="fill:url(#linearGradient1364);paint-order:fill markers stroke"/>
  <rect x="16.782" y="4.7961e-8" width="13.682" height="16.787" mask="url(#mask1250)" style="fill:url(#radialGradient11770-98);paint-order:fill markers stroke"/>
  <rect x="16.782" y="4.7961e-8" width="13.682" height="16.787" mask="url(#mask1244)" style="fill:url(#radialGradient11770-9-7);paint-order:fill markers stroke"/>
 </g>
</svg>
`;function p(s){let e;return{c(){e=o("main"),this.h()},l(r){e=k(r,"MAIN",{class:!0});var n=m(e);n.forEach(i),this.h()},h(){c(e,"class","svelte-1i1vchn")},m(r,n){f(r,e,n),e.innerHTML=h},p:a,i:a,o:a,d(r){r&&i(e)}}}class y extends t{constructor(e){super(),l(this,e,null,p,d,{})}}export{y as component};
